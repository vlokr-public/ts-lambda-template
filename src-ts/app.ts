import {
    APIGatewayProxyEvent,
    APIGatewayProxyResult
} from "aws-lambda";
import 'reflect-metadata';
import { Container } from "typedi";
import { Controller } from "./controllers/controller";
import { init } from "./loaders/index.loader";


export const lambdaHandler = async (
    event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {
    await init();
    return Container.get(Controller).invoke(event);
}