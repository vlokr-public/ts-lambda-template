import { APIGatewayProxyResult } from "aws-lambda";
import { Service, Inject } from 'typedi'


export interface ResponseBuilder {
    addStatusCode(statusCode: number): ResponseBuilder;
    addBody(body: string): ResponseBuilder;
    build(): APIGatewayProxyResult;
}

@Service()
export class ResponseBuilderImpl implements ResponseBuilder {
    private body: any;
    private statusCode: number;

    addStatusCode(statusCode: number): ResponseBuilder {
        this.statusCode = statusCode;
        return this;
    }
    addBody(body: any): ResponseBuilder {
        this.body =  body;
        return this
    }

    build(): APIGatewayProxyResult {
        return {
            statusCode: this.statusCode,
            body: JSON.stringify(this.body)
        }
    }
}