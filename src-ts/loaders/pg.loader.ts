import knex from "knex";
import { Client } from "pg";
import Container from "typedi";

export async function pgLoader() {

    const DATABASE_CREDENTIALS = {
        host: process.env.PGHOST,
        user: process.env.PGUSER,
        password: process.env.PGPASSWORD,
        database: process.env.PGDATABASE,
        port: process.env.PGPORT
    }

    const pgKnex = knex({
        client: 'pg',
        connection: DATABASE_CREDENTIALS,
        pool: { min: 0, max: 1 }
    })

    var client: Client = new Client();

    await client.connect();

    Container.set('pg.client', client);
    Container.set('pg.Knex', pgKnex);
}