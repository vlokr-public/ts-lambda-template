import { pgLoader } from "./pg.loader";

export async function init() {
    await pgLoader();
}