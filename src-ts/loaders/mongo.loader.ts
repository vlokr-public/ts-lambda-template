import { connect, model, Model } from 'mongoose'
import { Container } from 'typedi'

export async function loadDatabase() {
    try {
        await connect(process.env.MONGO_URI, { dbName: process.env.MONGO_DB, useNewUrlParser: true, useUnifiedTopology: true })
    } catch (err) {
        throw new Error(err);
    }
}

