import { APIGatewayProxyEvent, APIGatewayProxyResult } from "aws-lambda";
import Container, { Inject, Service } from "typedi";
import { ResponseBuilderImpl } from "../builder/builder";
import { RepositoryImpl } from "../domain/repository";

@Service()
export class Controller {

    builder = Container.get(ResponseBuilderImpl);

    constructor(private repository: RepositoryImpl) {
        this.builder.addStatusCode(200);
    }

    async invoke(event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> {

        try {
            const result = await this.repository.test()
            this.builder.addBody({data:result})
        } catch (error) {
            console.log(error);
            this.builder.addStatusCode(400);
            this.builder.addBody({ menssaje: "error in service" });
        }
        return this.builder.build();
    }
}