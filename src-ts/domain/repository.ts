import Container, { Inject, Service } from "typedi";
import { DataSourceImpl } from "../data/datasource";


@Service()
export class RepositoryImpl {
    constructor(private datasource: DataSourceImpl){ }

    async test(): Promise<Boolean> {
        return await this.datasource.test();
    }
}