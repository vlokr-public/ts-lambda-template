import { APIGatewayProxyEvent } from "aws-lambda";
import { describe, it } from "mocha";
import { config } from "dotenv";
import * as expect from "expect";
import { exit } from "process";

const lambdaHandler = require("../../src-ts/app");
const envConfig = config({ path: './test/unit/.env' });

if(envConfig.error){
    exit()
}

describe('Unit test for app handler', function () {
    it('verifies successful response', async () => {
        const event: APIGatewayProxyEvent = {
            queryStringParameters: {
                typeQuery: "followers"
            },
            pathParameters: {
                uidUser: "sLfbd0wdVJenmlqOdxRFPPUOykB2"
            }
        } as any
        const result = await lambdaHandler.lambdaHandler(event)

        expect(result.statusCode).toEqual(200);
    });
});